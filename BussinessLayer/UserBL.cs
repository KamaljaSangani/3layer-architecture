﻿using System;
using System.Collections.Generic;
using DataLayer;
using EntityLayer.ViewModels;

namespace BussinessLayer
{
    public class UserBL
    {
        UserDL userDL = new UserDL();

        public object VerifyAdmin(LoginVM loginVm)
        {

            var isExist = userDL.VerifyAdmin(loginVm);
            return isExist;
        }

        public List<UserVM> getUsers()
        {
            List<UserVM> userList = userDL.getUsers();
            return userList;
        }

        public void addUser(UserVM userVm)
        {
            userDL.addUser(userVm);
        }

        public UserVM findUser(int id)
        {
            UserVM user = userDL.findUser(id);
            return user;
        }

        public void editUser(UserVM userVm)
        {
            userDL.editUser(userVm);
        }

        public void deleteUser(int id)
        {
            userDL.deleteUser(id);
        }

    }
   
}
