﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EntityLayer.Model
{
    public class Login
    {
        public Login()
        {
        }

        [Key]
        public int LoginID { get; set; }

        //[Required(ErrorMessage = "UserName is required.")]
        public string UserName { get; set; }

        //[Required(ErrorMessage = "Password is required.")]
        //[DataType(DataType.Password)]
        //[RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,23}$", ErrorMessage = "Passwords must have at least eight character long including one non letter or digit character, at least one digit('0' - '9') and at least one uppercase('A' - 'Z').")]
        public string Password { get; set; }
    }
}
