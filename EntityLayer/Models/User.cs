﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EntityLayer.Model
{
    public class User
    {
        public User()
        {
        }
        [Key]
        public int UserID { get; set; }

        //[Required(ErrorMessage = "Name is required.")]
        public string Name { get; set; }

        //[Required(ErrorMessage = "Email is required.")]
        //[Display(Name = "Email ID")]
        //[DataType(DataType.EmailAddress)]
        //  [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage ="Please enter valid email.")]
        public string EmailID { get; set; }

        //[Required(ErrorMessage = "Phone number is required.")]
        //[Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        public DateTime BirthDate { get; set; }

        public string Gender { get; set; }
    }
}
