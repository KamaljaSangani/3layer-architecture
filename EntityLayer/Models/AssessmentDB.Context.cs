﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace EntityLayer.Model
{
    public class AssessmentEntities : DbContext
    {
        public AssessmentEntities()
                    : base("name=AssessmentEntities")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Login> Logins { get; set; }
    }
}
