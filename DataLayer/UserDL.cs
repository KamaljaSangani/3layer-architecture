﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using EntityLayer.Model;
using EntityLayer.ViewModels;

namespace DataLayer
{
    public class UserDL
    {
            AssessmentEntities db = new AssessmentEntities();

            public object VerifyAdmin(LoginVM loginVm)
            {
                var admin = db.Logins.SingleOrDefault(u => u.UserName == loginVm.UserName && u.Password == loginVm.Password);
                return admin;
            }

            public List<UserVM> getUsers()
            {
                List<User> users = db.Users.ToList();

                List<UserVM> usersVm = new List<UserVM>();

                foreach (User user in users)
                {
                    var config = new MapperConfiguration(cfg =>
                    cfg.CreateMap<User, UserVM>());
                    IMapper mapper = config.CreateMapper();
                    var userVm = mapper.Map<User, UserVM>(user);
                    usersVm.Add(userVm);
                }

                return usersVm;

            }

            public void addUser(UserVM userVM)
            {
                var config = new MapperConfiguration(cfg =>
                cfg.CreateMap<UserVM, User>());
                IMapper mapper = config.CreateMapper();
                var user = mapper.Map<UserVM, User>(userVM);

                db.Users.Add(user);
                db.SaveChanges();
            }

            public UserVM findUser(int id)
            {
                User user = db.Users.Find(id);

                var config = new MapperConfiguration(cfg =>
                    cfg.CreateMap<User, UserVM>());
                IMapper mapper = config.CreateMapper();
                UserVM userVm = mapper.Map<User, UserVM>(user);

                return userVm;
            }

            public void editUser(UserVM userVm)
            {
                var config = new MapperConfiguration(cfg =>
                cfg.CreateMap<UserVM, User>());
                IMapper mapper = config.CreateMapper();
                User user = mapper.Map<UserVM, User>(userVm);

                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
            }


            public void deleteUser(int id)
            {
                User user = db.Users.Find(id);
                db.Users.Remove(user);
                db.SaveChanges();
            }
        }
    
}
