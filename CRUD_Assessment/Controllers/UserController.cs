﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BussinessLayer;
using EntityLayer.ViewModels;

namespace CRUD_Assessment.Controllers
{
    public class UserController : Controller
    {
        UserBL userBL = new UserBL();
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginVM loginVm)
        {
            if (ModelState.IsValid)
            {

                var isExist = userBL.VerifyAdmin(loginVm);
                if (isExist != null)
                {
                    return RedirectToAction("ListUser");
                }
                else if (loginVm.UserName != null && loginVm.Password != null)
                {
                    ModelState.AddModelError("", "UserName or Password is wrong.");
                }
            }
            return View();
        }


        public ActionResult ListUser()
        {
            List<UserVM> userList = userBL.getUsers();

            return View(userList);
        }

        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(UserVM userVm)
        {
            if (ModelState.IsValid)
            {


                userBL.addUser(userVm);


                return RedirectToAction("ListUser");
            }

            return View();
        }


        public ActionResult Edit(int id)
        {
            UserVM user = userBL.findUser(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserVM userVm)
        {
            if (ModelState.IsValid)
            {
                userBL.editUser(userVm);
                return RedirectToAction("ListUser");
            }
            return View(userVm);
        }


        public ActionResult DeleteUser(int id)
        {
            userBL.deleteUser(id);
            return RedirectToAction("Index");
        }
    }
}
